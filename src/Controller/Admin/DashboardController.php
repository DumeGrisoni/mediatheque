<?php

namespace App\Controller\Admin;

use App\Entity\Employe;
use App\Entity\Livre;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $livre= $em->getRepository(Livre::class)->findAll();

        return $this->render('admin/dashboard.html.twig', [
            'livres' => $livre
        ]);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Mediathèque de la Chappel-Curreaux');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Espace Administration', 'fa fa-home');
         yield MenuItem::linkToCrud('Employés', 'fas fa-users', Employe::class);
         yield MenuItem::linkToCrud('Livres', 'fas fa-book', Livre::class);
    }
}
