<?php

namespace App\Controller;

use App\Entity\Livre;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogueController extends AbstractController
{
    /**
     * @param Livre
     * @Route("/catalogue", name="catalogue")
     * @return Response
     */
    public function index(): Response
    {
        $em = $this->getDoctrine()->getManager();
        $livre= $em->getRepository(Livre::class)->findAll();

        return $this->render('catalogue/index.html.twig', [
            'livres' => $livre
        ]);
    }
}
